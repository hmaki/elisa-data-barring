#!/usr/bin/sh

export ELISA_USER=your_email_address_for_elisa
export ELISA_PW=your_password_for_elisa
export GMAIL_USER=your_gmail_address
export GMAIL_PW=your_gmail_password

/usr/bin/python3 /home/user/elisa-data-barring/elisa_data_barring.py "$@"
