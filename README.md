# elisa data barring

This Python script helps to automate setting cellular data on and off from e.g. children's phone under Elisa subscription. Elisa is a Finnish mobile operator. 

Script logs into Elisa's online service with your credentials, sets data barring on/off for mobile subriptions of your choosing (and under your ownership), logs out, and finally sends an email report to your email address. The script is using REST API that was found out by examining how Elisa's normal web front works.

## Synopsis

```
usage: elisa_data_barring.py [-h] [--off] file

positional arguments:
  file        file containing targeted subscriptions

optional arguments:
  -h, --help  show this help message and exit
  --off       sets data barring off
```

## How to run

To run the Python script, you need to provide a file that lists the subscription IDs (separated by newline) for which the data barring will be applied. The subscription IDs can be found from Elisa's online service.

``--off`` argument sets barring off, i.e. restores to normal operation.

You'll also need to have the credentials for Elisa's online service and Gmail in environment variables. Gmail, because the script is using Google's SMTP service.

For setting data barring on/off automatically, it's useful to run it from cron. Cron jobs are run without environment variables, so you need to make sure the needed variables are set before the script is run.
One way is to let cron run a tiny Bash script that sets the environment variables and then calls the Python script (see cron_run_sample.sh and crontab_sample.txt).


## Caveats

Mobile data doesn't seem to resume immediately when data barring is set off. It resumes if phone is put to flight mode and then back to normal, or through restart. A nuisance, but at least kids are not spending nights with their phones.

Also, make sure nobody else is able to read the Bash script that contains the credentials.