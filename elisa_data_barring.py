#!/usr/bin/python3

import urllib.request, urllib.error
import json
import argparse
import ssl
import os
import smtplib
from datetime import datetime

# set up argument parser
parser = argparse.ArgumentParser()
parser.add_argument('--off', action='store_true', help='sets data barring off' )
parser.add_argument('file', type=argparse.FileType('r'), help='file containing targeted subscriptions')
args = parser.parse_args()

# read subscription numbers from file
with args.file as file:
    numbers = [line.rstrip('\n') for line in file]

# set usernames and password from environment variables
try:
    username = os.environ['ELISA_USER']
    password = os.environ["ELISA_PW"]
    gmail_user = os.environ['GMAIL_USER']
    gmail_password = os.environ['GMAIL_PW']
except KeyError: 
    print("Set the environment variables.")
    exit(1)

# elisa's API endpoints
url_elisa = 'https://verkkoasiointi.elisa.fi'
url_auth = url_elisa + '/rest/public/login/password-auth-code/' + username
url_sso_login = 'https://id.elisa.fi/sso/login?client_id=ipa&language=fi'
url_login = url_elisa + '/rest/public/login'
url_subscription = url_elisa + '/rest/mobile/contracts/XXX/mobile-subscription'
url_barrings = url_elisa + '/rest/mobile/contracts/XXX/mobile-subscription/barrings'
url_inhibition = url_elisa + '/rest/mobile/XXX/inhibition'
url_logout = url_elisa + '/rest/public/logout'

cookie = ''
msg_body = ''

# set HTTPS debugger
#opener = urllib.request.build_opener(urllib.request.HTTPSHandler(debuglevel = 10))
#urllib.request.install_opener(opener)


# function that collects report to be sent as email
def output(str):
    global msg_body

    msg_body += str
    print(str, end='')
    return


# function that sends output report to email
def send_report(subject):
    try:
        message = 'Subject: {}\n\n{}'.format(subject, msg_body)
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(gmail_user, username, message)
        server.close()

        print('Email sent!')
    except Exception as e:
        print('Sendmail error: ' + str(e))
    return


# function that sends request, handles exceptions and outputs some statuses
def send_request(req, *data):
    try:
        req.add_header('Referer', url_elisa)
        if cookie:
            req.add_header('Cookie', cookie)
        output(req.get_method() + " " + req.selector + " --> ")
        if data:
            req.add_header('Content-Type', 'application/json')
            resp = urllib.request.urlopen(req, data)
        else:
            resp = urllib.request.urlopen(req)
    except urllib.error.HTTPError as e:
        output(str(e.code) + " " + e.reason + '\n')
        exit(1)
    except urllib.error.URLError as e:
        output("URLError: " + str(e.reason) + '\n')
        exit(1)
    else:
        output(str(resp.getcode()) + '\n')
        return resp


# function that logs into elisa verkkoasiointi
def login():
    global cookie

    # request auth code and extract it from json response
    req = urllib.request.Request(url_auth)
    resp = send_request(req)
    auth_code = json.loads(resp.read().decode())['code']

    # prepare json payload with credentials and auth code
    json_data = {}
    json_data['accountId'] = username
    json_data['password'] = password
    json_data['authCode'] = auth_code
    json_data_bytes = json.dumps(json_data).encode('utf-8')

    # send sso login request and extract authorization token from json response
    req = urllib.request.Request(url_sso_login)
    resp = send_request(req, json_data_bytes)
    token = json.loads(resp.read().decode())['token']

    # prepare json paylaod with authorization token
    json_data = {}
    json_data['token'] = token
    json_data_bytes = json.dumps(json_data).encode('utf-8')

    # send login request and extract session cookie from response headers
    req = urllib.request.Request(url_login)
    resp = send_request(req, json_data_bytes)
    cookie = dict(resp.info())['Set-Cookie'].split(';')[0]
    return


# function that sets data barring on and off
def set_data_barring():
    for num in numbers:
        # request current barring settings
        req = urllib.request.Request(url_subscription.replace("XXX", num))
        resp = send_request(req)

        # set new data barring
        json_data = json.loads(resp.read().decode())['barring']
        json_data['operationType'] = 'DATA_BARRING'
        if args.off:
            json_data['dataBarring']['dataBarringSet'] = False
        else:
            json_data['dataBarring']['dataBarringSet'] = True
        json_data_bytes = json.dumps(json_data).encode('utf-8')

        # put new barring settings
        req = urllib.request.Request(url_barrings.replace("XXX", num), method='PUT')          
        resp = send_request(req, json_data_bytes)
    return

# function that sets mobile inhibition on (for outbound) and off
def set_inhibition():
    # prepare json payload with inhibition status.
    inhibition_data = '{"inhibitionType":"NONE","operationType":"MOBILE_INHIBITION"}'
    json_data = json.loads(inhibition_data)

    if args.off:
        json_data['inhibitionType'] = 'NONE'
    else:
        json_data['inhibitionType'] = 'OUTBOUND' # blocks all outbound traffic, also calls!
    json_data_bytes = json.dumps(json_data).encode('utf-8')

    # put barring setting
    for num in numbers:
        req = urllib.request.Request(url_inhibition.replace("XXX", num), method='PUT')          
        resp = send_request(req, json_data_bytes)    
    return


# function that logs out from elisa verkkoasiointi
def logout():
    req = urllib.request.Request(url_logout, method='POST')
    resp = send_request(req)
    return


### main ###
try:
    login()
    #set_data_barring()
    set_inhibition()    
    logout()

    if args.off:
        state = 'OFF'
    else:
        state = 'ON'

    time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    output('\nData barring was set %s at %s.\n' % (state, time))
    send_report('Data barring %s' % (state))
    exit(0)
except Exception as e:
    time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    output('\nData barring failed at %s.\nReason: %s\n' % (time, str(e)))
    send_report('Data barring FAILED')
    exit(1)
